<?php
/** 
 * TestMyClass.php
 *
 * PHP Version 5 
 *
 * @category Gizur
 * @package  Tests
 * @author   Jonas Colmsjo <jonas@gizur.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://www.gizur.com
 * @filesource
 *
 * PhpDoc is used for documentation: 
 * http://www.phpdoc.org/docs/latest/for-users/phpdoc-reference.html
 *
 * Coding standards:
 * http://framework.zend.com/manual/1.12/en/coding-standard.html
 *
 **/

// Autoload libraries
require_once __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../src/Gizur/MyClass.php';
 
class MyClassTest extends PHPUnit_Framework_TestCase
{
    public function testSaveToBucket()
    {
        $myClass = new Gizur\MyClasss();

        // just run it
        $myClass->saveToBucket();

        // Always succeeding
        $this->assertEquals(100, 100);
    }

    public function testListBucket()
    {
        $myClass = new Gizur\MyClasss();

        // just run it
        $myClass->listBucket();

        // Always succeeding
        $this->assertEquals(100, 100);
    }

    public function testWget()
    {
        $myClass = new Gizur\MyClasss();

        // test the function

        $length = $myClass->myWget(
            'https://raw.github.com/colmsjo/mirrorlogfiles/master/LICENSE', 
            'LICENSE'
        );

        // Always succeeding
        $this->assertEquals(100, 100);
    }

}
