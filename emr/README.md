MR Grep in logiles with Elastic Map Reduce
=========================================


Preparations
------------

First create a Python virtual environment using virtualenv:

```
# python-env is a suggested name, any name can be used
virtualenv python-env
```

A S3 command line tool is good to have when working extensively with S3. The most widely
used one is probably s3cmd (part of s3tools).

This tool can be installed in many ways, one alternative is to use pip (requirements.txt cotains the
dependencies to install):

```
cp requirements.txt python-env && cd python-env
./bin/pip install -r requirements.txt
cd ..
```

Then setup the credentials:

```
./python-env/bin/s3cmd --configure --config=./s3cfg

# Update the configuration just created
# For instance, make sure that bukcet-location is set correctly (US (default), EU, us-west-1, and ap-southeast-1)
nano ./s3cfg

# Now you should be able to list the buckets
./python-env/bin/s3cmd --config=./s3cfg ls
```


An alternative is to use the node package s3cli (package.json has been prepared for this):

```
npm install
```


List available fabfile commands:

```
./python-env/bin/fab -f ./fabfile.py -l
```

This alias is usefull to have:

```
# Define a alias for fab, you need to be in the repo root folder to run it
# Add this to your bashrc (or other shell rc) file 
alias fab='./python-env/bin/fab -f ./fabfile.py'

# Now just run fab
fab list
```



Setup
-----


1. A credentials file needs to be created:

```
Gizur-Laptop-5:MapReduce jonas$ cat credentials.json 
  {
    "access-id": "xxx",
    "private-key": "yyy",
    "key-pair": "keypair1",
    "key-pair-file": "keypair1.pem",
    "log_uri": "s3n://gc4-emr-sbx/",
    "region": "eu-west-1"
  }

```

1. Run a simple test after the credentials.json has been created. The output is empty:

```
./python-env/bin/fab -f ./fabfile.py list_jobs

# or if you prefer
elastic-mapreduce -c credentials.json --list
```


1. Update config.py if you chose another name than python-env for your virtual environment. You should also change
the name of the bucket here.


1. Copy the script to run to S3. A fab task has been prepared for this (the bucket will also be created):

```
./python-env/bin/fab -f ./fabfile.py copy_script
```


1. Now we're finally ready to run some hadoop (EMR) jobs:


```
# Start a new job flow
./python-env/bin/fab -f ./fabfile.py create_job

# Will do this
elastic-mapreduce --create --alive


# List running job flows
./python-env/bin/fab -f ./fabfile.py list_jobs
j-3JNIPPAJX24ZG     STARTING                                                         Development Job Flow (requires manual termination)


# Terminate job flow like this (you need at least one job flow below)
./python-env/bin/fab -f ./fabfile.py term_job:id=<job stream id>
Terminated job flow <job stream id>

# Same as this
elastic-mapreduce --terminate <job stream id>

```

1. Now update `config.py` with the job flow id for the job created above

```
nano config.py
```

1. Add a step to the running job:

```
./python-env/bin/fab -f ./fabfile.py add_job_step

# Same as this
elastic-mapreduce -j JobFlowID --stream \
                  --mapper  s3://<BUCKET>/map_reduce_logs.py \ 
                  --input   s3://<BUCKET> \
                  --output  s3://<BUCKET>/output \
                  --reducer aggregate
```





