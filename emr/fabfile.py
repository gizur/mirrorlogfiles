# -*- coding: utf-8 -*-

from fabric.api import local

#
# This is a small fabfile developed for learning purposes
#
# Examples: 
# >fab prepare_deploy
# >fab hello:name=MYNAME
#


import config


#
# Variables used
#

EMR_PATH = config.REPO_PATH + "/emr"
PY_PATH  = EMR_PATH + config.ENV


##############################################################################
#
# Coding tasks
#

#
# Run unit tests
#
# An alternative way to set PYTHONPATH is to edit bin/activate
#

def test():
    global ENV
    local("PYTHONPATH=tests ./"+config.ENV+"/bin/python -m test1")


#
# Simple test just running the python scripts
#

def simpletest():
    local("echo 'run this:sudo cat /var/log/*|./"+config.ENV+"/bin/python map_logs.py|sort|./"+config.ENV+"/bin/python reduce_logs.py'")
    local("sudo cat /var/log/*|./"+config.ENV+"/bin/python map_logs.py|sort|./"+config.ENV+"/bin/python reduce_logs.py")


#
# Generate documentation
#

def doc():
    global ENV
    local("./"+config.ENV+"/bin/pycco *.py")
    local("cd pydoc && ../"+config.ENV+"/bin/python -m pydoc -w  ../*.py")


#
# Check code quality
#

def lint():
    global ENV
    local("./"+config.ENV+"/bin/pylint tests/*.py")


#
# Clean up
#

def clean():
    local("rm -rf docs && rm -rf pydoc")
    local("mkdir docs && mkdir pydoc")
    local("rm -rf logs && mkdir logs")
    local("rm -rf output && mkdir output")


#----------------------------------------------------------------------------


#
# Start local hadoop task
#

def start_local_hadoop():
    global LOCAL_HADOOP_STARTUP_SCRIPT
    local(config.LOCAL_HADOOP_STARTUP_SCRIPT)


#
# Run local example hadoop task
#

def run_local_example_job():
    global LOCAL_HADOOP_FS_PATH, LOCAL_HADOOP_STREAMING_PATH
    local("hadoop jar " + config.LOCAL_HADOOP_STREAMING_PATH + " " +
          "-file    " + EMR_PATH + "/test/mapper.py "+
          "-mapper  " + EMR_PATH + "/test/mapper.py "+
          "-file    " + EMR_PATH + "/test/reducer.py "+
          "-reducer " + EMR_PATH + "/test/reducer.py "+
          "-input   " + config.LOCAL_HADOOP_FS_PATH + "/wordcount/* "+
          "-output  " + config.LOCAL_HADOOP_FS_PATH + "/wordcount-output")


#
# Copy local example input
#

def copy_local_example_input():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -put ./test/zaratustra.txt "+config.LOCAL_HADOOP_FS_PATH+"/wordcount")


#
# Remove example output folder in order to be able to run again
#

def remove_local_example_output():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -rmr "+config.LOCAL_HADOOP_FS_PATH+"/wordcount-output");


#
# Show output from exmaple job
# 

def cat_local_example_output():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -cat "+config.LOCAL_HADOOP_FS_PATH+"/wordcount-output/*");


#----------------------------------------------------------------------------

#
# Create local input dir
#

def mkdir_local_input():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -mkdir "          + config.LOCAL_HADOOP_FS_PATH + "/logs")


#
# Copy local  input
#

def copy_local_input():
    global LOCAL_HADOOP_FS_PATH
    local("sudo hadoop fs -put /var/log/* " + config.LOCAL_HADOOP_FS_PATH + "/logs")


#
# Run local  hadoop task
#

def run_local_job():
    global LOCAL_HADOOP_FS_PATH, LOCAL_HADOOP_STREAMING_PATH
    local("hadoop jar " + config.LOCAL_HADOOP_STREAMING_PATH + " " +
          "-file    " + EMR_PATH + "/map_logs.py "+
          "-mapper  " + EMR_PATH + "/map_logs.py "+
          "-file    " + EMR_PATH + "/reduce_logs.py "+
          "-reducer " + EMR_PATH + "/reduce_logs.py "+
          "-input   " + config.LOCAL_HADOOP_FS_PATH + "/logs/* "+
          "-output  " + config.LOCAL_HADOOP_FS_PATH + "/logs-output")


#
# Remove example output folder in order to be able to run again
#

def remove_local_output():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -rmr "+config.LOCAL_HADOOP_FS_PATH+"/logs-output");


#
# Show output from exmaple job
# 

def cat_local_output():
    global LOCAL_HADOOP_FS_PATH
    local("hadoop fs -cat "+config.LOCAL_HADOOP_FS_PATH+"/logs-output/*");


##############################################################################
#
# AWS S3 tasks
#


#
# Create bucket
#

def create_bucket():
    global ENV, BUCKET
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg mb s3://"+config.BUCKET)
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg mb s3://"+config.LOG_BUCKET)


#
# Copy script to s3
#

def copy_scripts():
    global ENV, BUCKET
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg put ./map_logs.py ./reduce_logs.py s3://"+config.BUCKET)
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg ls s3://"+config.BUCKET)


#
# Show logs
#

def list_logs():
    global ENV, BUCKET, LOG_BUCKET, JOBFLOWID
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg ls s3://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+"/steps/")
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg ls s3://"+config.LOG_BUCKET+"/output/")


#
# Show logs
#

def s3_ls(dir):
    global ENV, BUCKET, LOG_BUCKET, JOBFLOWID, REPO_PATH
    local(PY_PATH+"/bin/s3cmd --config="+PY_PATH+"/../s3cfg ls s3://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+dir)


#
# get the logs and the output
#

def get_logs():
    global ENV, BUCKET, LOG_BUCKET, JOBFLOWID
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg get --force --recursive s3://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+"/steps/ logs")
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg get --force --recursive s3://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+"/output/ output")


#
# Remove output
#

def remove_output():
    global ENV, BUCKET, LOG_BUCKET, JOBFLOWID
    local("./"+config.ENV+"/bin/s3cmd --config=./s3cfg del --force --recursive s3://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+"/output")


##############################################################################
#
# Elastic Map Reduce tasks
#

EMR_PARMS =     config.EXTRA_EMR_ARGS + \
                " --mapper  s3n://" + config.BUCKET + "/map_logs.py "+ \
                " --input   s3n://" + config.BUCKET + config.LOGS_PATH + \
                " --output  s3n://" + config.LOG_BUCKET + "/"+config.JOBFLOWID+"/output"+ \
                " --reducer s3n://" + config.BUCKET + "/reduce_logs.py"


#
# Create EMR stream
#

def create_job():
    local("elastic-mapreduce --create --alive")
    local("echo 'NOTE: Please remember to update config.py with the job flow id'")


#
# Create hbase EMR stream
#

def create_hbase_job():
    local("elastic-mapreduce --create --hbase --instance-type m1.large --alive")
    local("echo 'NOTE: Please remember to update config.py with the job flow id'")


#
# Terminate EMR stream
#

def term_job(id):
    local("elastic-mapreduce --terminate "+id)


#
# List EMR streams
#

def list_jobs():
    local("elastic-mapreduce --list --active")


#
# List EMR streams
#

def list_all_jobs():
    local("elastic-mapreduce --list")


#
# Add step to existing job stream
#

def add_job_step():
    global ENV, BUCKET, JOBFLOWID
    local("elastic-mapreduce -j "+config.JOBFLOWID+" --stream "+EMR_PARMS)


#
# Run MR job in new stream
#

def run_job():
    global ENV, BUCKET, JOBFLOWID
    local("elastic-mapreduce --create --stream "+EMR_PARMS)

#
# Run Example MR job in new stream
#

def run_example_job():
    global ENV, BUCKET, JOBFLOWID
    local("elastic-mapreduce --create --stream "+
            "  --mapper  s3://elasticmapreduce/samples/wordcount/wordSplitter.py "+
            "  --input   s3://elasticmapreduce/samples/wordcount/input "+
            "  --output  s3n://"+config.LOG_BUCKET+"/"+config.JOBFLOWID+"/output"+
            "  --reducer aggregate")


#
# Ssh into the ec2 instance
#

def ssh():
    global ENV, BUCKET, LOG_BUCKET, JOBFLOWID
    local("elastic-mapreduce --ssh --jobflow "+config.JOBFLOWID)
