#!/usr/bin/env python

"""
map_reduce_logs.py

Copyright (c) Jonas Colmsjo

Coding standards - http://www.python.org/dev/peps/pep-0008/

#
# error: This is a low cap err
# ERROR: This is a upper cap err
# warning: This is a low cap warn
# WARNING: This is a upper cap warni

"""

__author__   =  'Jonas Colmsjo'
__version__  =  '0.1'
__nonsense__ = 'colmsjo'


# Used for reading files
import sys

# Using regular expressions
import re

# used to access os.environ
import os

class MapReduce:
    """
    Mapper and Reducer
    ==================
    """

    def map(self):
        """
        filter out errors and warnings
        --------------------
        """
        for line in sys.stdin.readlines():
            m = re.search('(error|warning)', line.lower())
            if m: 
                filename = os.environ["map_input_file"]  if ("map_input_file"  in os.environ) else  ""
                startpos = os.environ["map_input_start"]  if ("map_input_start"  in os.environ) else  ""
                print filename + ":"  startpos + ":" + ":" + line,



if __name__ == '__main__':
    mr = MapReduce();
    mr.map();
