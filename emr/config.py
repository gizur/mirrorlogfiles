"""
config.py

Copyright (c) Jonas Colmsjo

Coding standards - http://www.python.org/dev/peps/pep-0008/


"""

#
# Path to the root of the repo
#

REPO_PATH = '/Users/jonas/git/colmsjo/mirrorlogfiles'


#
# virtualenv path, update if the suggested path isn't used
#

ENV = 'python-env'


#
# Bucket to use for the jobs. Should be the same as the bucket in ../config.inc.php
# (unless you have some job that copies the files to this bukcet)
#

BUCKET     = 'gc4-logs1'
LOG_BUCKET = 'gc4-emr-logs'


#
# The name of the stream used is placed here
# NOTE: Should find a better solution...
#

JOBFLOWID = 'j-24TYN5FEL0B7V'


#
# Additional parameters to pass to EMR when creating jobs
# for instance: --enable-debugging 
#

EXTRA_EMR_ARGS = ''

#
# Path to the logs to analyze - NOT USED
#

LOGS_PATH = '/*'


#
# Local hadoop installation
#

LOCAL_HADOOP_STARTUP_SCRIPT   = '/usr/local/Cellar/hadoop/1.1.1/bin/start-all.sh'
LOCAL_HADOOP_STREAMING_PATH   = '/usr/local/Cellar/hadoop/1.1.1/libexec/contrib/streaming/hadoop-streaming-1.1.1.jar'
LOCAL_HADOOP_FS_PATH          = '/Users/jonas/hadoop-store/mapred'


