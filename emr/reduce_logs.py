#!/usr/bin/env python

"""
map_reduce_logs.py

Copyright (c) Jonas Colmsjo

Coding standards - http://www.python.org/dev/peps/pep-0008/

#
# error: This is a low cap err
# ERROR: This is a upper cap err
# warning: This is a low cap warn
# WARNING: This is a upper cap warni

"""

__author__   =  'Jonas Colmsjo'
__version__  =  '0.1'
__nonsense__ = 'colmsjo'


# Used for reading files
import sys

# Using regular expressions
import re


class MapReduce:
    """
    Mapper and Reducer
    ==================
    """


    def reduce(self):
        """
        just echo stdin to stdout
        --------------------------
        """
        for line in sys.stdin.readlines():
            print line,



if __name__ == '__main__':
    mr = MapReduce();
    mr.reduce();
