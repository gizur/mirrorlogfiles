<?php
/** 
 * wget.php
 *
 * PHP Version 5 
 *
 * @category Gizur
 * @package  Utils
 * @author   Jonas Colmsjo <jonas@gizur.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://www.gizur.com
 * @filesource
 *
 * PhpDoc is used for documentation: 
 * http://www.phpdoc.org/docs/latest/for-users/phpdoc-reference.html
 *
 * Coding standards:
 * http://framework.zend.com/manual/1.12/en/coding-standard.html
 *
 * NOTE:
 * All dependencies needs to be loaded with `composer install`
 * composer.json should include all dependencies
 *
 **/

namespace Gizur;

// Autoload libraries
require_once __DIR__ . '/../../vendor/autoload.php';

// Configuration
//require __DIR__ . '/config.inc.php';
require __DIR__ . '/../../config.inc.php';

// Use AWS namespace
use Aws\Common\Aws;
use Aws\Common\Enum\Region;
use Guzzle\Stream\Stream;

// Use REST Client
use Guzzle\Http\Client;


/**
 * MyClass
 *
 * Get file via HTTP, like wget
 */

class MyClasss
{

    function __construct() 
    {
        global $automaticUpdates;

        // Check if automatic updates should be performed
        if ($automaticUpdates) {
            $this->updateSource();
        }
    }


    /**
     * Update the git repository with the source
     */
    public function updateSource()
    {
        global $automaticUpdates;

        if ($automaticUpdates) {
            echo "Updating library..." . PHP_EOL;
            exec("git pull");
        }
    }


    private function getAWS() 
    {

        global $region;

        $key    = getenv('AWS_API_KEY');
        $secret = getenv('AWS_API_SECRET');

        // Make sure that we have login credentials
        if (!$key || !$secret) {

            $ini = False;

            if (file_exists('/etc/php.d/environment.ini')) {
                // Try to use the Elastic Beanstalk ini file
                $ini = parse_ini_file('/etc/php.d/environment.ini');
            } elseif (file_exists(__DIR__ .'/../../.env')) {
                $ini = parse_ini_file(__DIR__ .'/../../.env');
            } else {
                echo "Environment variables AWS_API_KEY and AWS_API_SECRET " .
                     "needs to be set" . PHP_EOL;
                
                return False;
            }

            if ($ini && 
                array_key_exists("aws.access_key", $ini) && 
                array_key_exists("aws.secret_key", $ini)) {
                
                $key    = $ini["aws.access_key"];
                $secret = $ini["aws.secret_key"];
            } else {
                echo "Environment variables AWS_API_KEY and AWS_API_SECRET " .
                     "needs to be set" . PHP_EOL;
                
                return False;
            }
        }

        // Create an Aws object
        $aws = Aws::factory(
            array(
               'key'    => $key,
               'secret' => $secret,
               'region' => $region
            )
        );

        return $aws;
    }


    /**
     * Save the directories with log files to the bucket
     */

    public function saveToBucket()
    {
        global $region, $paths, $bucket, $filenamePrefix;

 
        if (!$aws = $this->getAWS()) {
            return;
        }

        // Create a S3 client
        $sThree = $aws->get('s3');


        /** Upload file to bucket */

        echo 'Saving log files to S3 in progress...' . PHP_EOL;

        // Iterate over all the paths that should be saved
        foreach ($paths as $path) {

            // List all files mathing the pattern
            $files = glob($path);

            // Iterate over all the files
            foreach ($files as $file) {

                // Save the file with the hostname and filenamePrefix as prefix
                $fullPath = $filenamePrefix . "__" . gethostname() . $file;

                // The analysis of the log files is easier to do without folders
                $key = str_replace('/', '__', $fullPath);

                $fileHandle = fopen($file, 'r');

                // Make sure that the file exists before trying to read
                if ($fileHandle) {
                    // echo "Saving " . $key . PHP_EOL;

                    // Save the file to S3
                    $model = $sThree->putObject(
                        array(
                        'Bucket' => $bucket,
                        'Key'       => $key,
                        'Body'      => new Stream($fileHandle)
                        )
                    );

                    echo "Status saving" . $key . " with status: " . 
                         implode(",", $model->getAll()) . PHP_EOL;
                }
            }
        }
    }

    /**
     * List the contents of the bucket used (set in config.inc.php)
     */

    public function listBucket()
    {
        global $region, $paths, $bucket;

        // Create an Aws object
        if (!$aws = $this->getAWS()) {
            return;
        }

        // Create a S3 client
        $sThree = $aws->get('s3');


        /* List contents of bucket */
 
        echo 'Current content of target bucket...' . PHP_EOL;

        $listObjectsCommand = $sThree->getCommand(
            'ListObjects', array(
            'Bucket' => $bucket
            )
        );


        $iterator = $sThree->getIterator($listObjectsCommand);

        foreach ($iterator as $object) {
            echo $object['Key'] . ' - ' . $object['Size'] . PHP_EOL;
        }

    }

    /**
     * Fetch a file over HTTP and save as a local file
     *
     */
    public function myWget($url, $saveAs) 
    {
        global $region, $paths, $bucket;

        /** Create HTTP client */
        $client = new Client($url);

        /** Get the file */
        $request  = $client->get('');
        $response = $request->send();

        $response->getBody();

        /** Save file locally */
        file_put_contents($saveAs, $response->getBody());

        return $response->getHeader('Content-Length');
    }

}

