Mirror Log Files
=================

Simple script for saving log files to S3. The installation in performed using [composer](http://getcomposer.org/download/).

Installation is simple, just do `curl -sS https://raw.github.com/colmsjo/mirrorlogfiles/master/install.sh | sh`
The installation should be performed in a empty directory.

Copy `config.inc.php.template` to `config.inc.php`. Open and set the bucket to use. 

The following environment variables needs to be set:

 * AWS_API_KEY - Your account key for AWS
 * AWS_API_SECRET - Your account secret for AWS

The script will also check if the Elastic Beanstalk varaibles that are set in the Container. 
The file .env can also be used as a last resort.

```
aws.secret_key = yyyy
aws.access_key = xxxx
```

Then just run `./bin/savelogs`. You can see the content of the bucket with `./bin/listbucket`


Troubleshooting
---------------

1. Problems installing composer. For instance in Elastic Beanstalk.

```
The apc.enable_cli setting is incorrect.
Add the following to the end of your `php.ini`:
    apc.enable_cli = Off
```

Resolution `sudo sh -c 'echo -e "\napc.enable_cli = Off" >> /etc/php.ini'`. Then run the installatio
again.




For developers
-------------

 * Install locally: `make install` (assuming composer has been installed globally, run php composer.phar install otherwise)
  * In case of changes to composer.json do: `composer update`
 * Run unit tests: `make test`
  * Also runs code sniffer
 * Generate documentation `make doc`


