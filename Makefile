all:

install:
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar composer
	./composer install

clean:
	rm -rf build cache

test:
	./vendor/bin/phpunit tests/TestMyClass.php 
	./vendor/bin/phpcs --standard=Zend src/**/*.php tests/*.php

lint:
	./vendor/bin/phpcs --standard=Zend src/**/*.php tests/*.php

docs:
	php ./vendor/bin/sami.php update --force sami-config.php	
